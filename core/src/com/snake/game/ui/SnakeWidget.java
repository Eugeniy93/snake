package com.snake.game.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.utils.Disposable;
import com.snake.game.engine.SnakeGame;
import com.snake.game.util.Point;

public class SnakeWidget extends Widget implements Disposable {
    private static final int DEFAULT_BASE_SIZE = 10;
    private static final Color SNAKE_COLOR = new Color(0x33ff00ff); // TODO хардкод -> ресурсы
    private static final Color FOOD_COLOR = new Color(0xfe0000ff); // TODO хардкод -> ресурсы

    private SnakeGame snakeGame;
    private Texture cellTexture;
    private final int baseSize;
    private boolean initialized = false;

    private float timer = 0;
    private float time = 0;

    private SnakeWidgetListener listener;
    private final Point cellSize = new Point();

    private final Color tmpSnakeColor = new Color();

    public SnakeWidget() {
        this(DEFAULT_BASE_SIZE);
    }

    public SnakeWidget(int baseSize) {
        this.baseSize = baseSize;
    }

    public SnakeWidgetListener getListener() {
        return listener;
    }

    public void setListener(SnakeWidgetListener listener) {
        this.listener = listener;
    }

    public SnakeGame getSnakeGame() {
        return snakeGame;
    }

    public Point getCellSize() {
        return cellSize;
    }

    public void setSnakeGame(SnakeGame snakeGame) {
        this.snakeGame = snakeGame;
        this.initialized = false;
    }

    private void initialize() {
        cellSize.set((int) (getWidth() / snakeGame.getColumns()), (int) (getHeight() / snakeGame.getLines()));

        final Pixmap pixmap = new Pixmap(cellSize.x, cellSize.y, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        if (cellTexture != null) cellTexture.dispose();
        cellTexture = new Texture(pixmap);

        this.initialized = true;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        time += delta;
        timer += delta;
        if (timer >= getStepRate()) {
            timer = 0;
            snakeGame.step();
            if (listener != null) {
                listener.onStep();
            }
        }
        if (snakeGame.isGameOver()) {
            if (listener != null) {
                listener.onGameOver();
            }
        }
    }

    private float getStepRate() {
        return 1.0f / snakeGame.getSnake().getSegments().size();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (!initialized) {
            initialize();
        }

        for (int i = 0, l = snakeGame.getSnake().getSegments().size(); i < l; i++) {
            tmpSnakeColor.set(SNAKE_COLOR);
            float begin = 100;
            float end = 40;
            float k = (begin - i * (begin - end) / (l - 1)) / 100.0f;
            tmpSnakeColor.a = k;
            batch.setColor(tmpSnakeColor);
            drawBlock(batch, snakeGame.getSnake().getSegments().get(i));
        }

        batch.setColor(FOOD_COLOR);
        drawBlock(batch, snakeGame.getFood());
    }

    private void drawBlock(Batch batch, Point position) {
        batch.draw(cellTexture, position.x * cellSize.x, position.y * cellSize.y);
    }

    @Override
    public void dispose() {
        cellTexture.dispose();
    }

    public int getBaseSize() {
        return baseSize;
    }

    public interface SnakeWidgetListener {
        void onStep();

        void onGameOver();
    }
}
