package com.snake.game.ext;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.action.ActorConsumer;
import com.github.czyzby.lml.parser.impl.DefaultLmlSyntax;
import com.github.czyzby.lml.parser.impl.attribute.table.cell.*;
import com.github.czyzby.lml.parser.impl.tag.AbstractNonParentalActorLmlTag;
import com.github.czyzby.lml.parser.tag.*;
import com.snake.game.ui.SnakeWidget;
import com.snake.game.util.ResourceService;

public class ExtLmlSyntax extends DefaultLmlSyntax {

    public ExtLmlSyntax() {
        addBuildingAttributeProcessor(new SnakeSizeLmlAttribute(), "size");

        addTagProvider(new SnakeLmlTagProvider(), "snake");

        addAttributeProcessor(new ResLmlAttribute(), "res");

        addCellAttributeProcessor(new LmlAttributeDpValueWrapper(new CellPadBottomLmlAttribute()), "padBottom");
        addCellAttributeProcessor(new LmlAttributeDpValueWrapper(new CellPadLeftLmlAttribute()), "padLeft");
        addCellAttributeProcessor(new LmlAttributeDpValueWrapper(new CellPadLmlAttribute()), "pad");
        addCellAttributeProcessor(new LmlAttributeDpValueWrapper(new CellPadRightLmlAttribute()), "padRight");
        addCellAttributeProcessor(new LmlAttributeDpValueWrapper(new CellPadTopLmlAttribute()), "padTop");
    }

    /** Provides Button tags.
     *
     * @author MJ */
    public class SnakeLmlTagProvider implements LmlTagProvider {
        @Override
        public LmlTag create(final LmlParser parser, final LmlTag parentTag, final StringBuilder rawTagData) {
            return new SnakeLmlTag(parser, parentTag, rawTagData);
        }
    }

    /** Handles {@link Button} actor. Allows its children tags to use cell attributes. Mapped to "button".
     *
     * @author MJ */
    public class SnakeLmlTag extends AbstractNonParentalActorLmlTag {
        public SnakeLmlTag(final LmlParser parser, final LmlTag parentTag, final StringBuilder rawTagData) {
            super(parser, parentTag, rawTagData);
        }

        @Override
        protected LmlActorBuilder getNewInstanceOfBuilder() {
            return new SnakeLmlActorBuilder();
        }

        @Override
        protected Actor getNewInstanceOfActor(LmlActorBuilder builder) {
            final SnakeLmlActorBuilder b = (SnakeLmlActorBuilder) builder;
            final int size = b.getSize();
            return new SnakeWidget();
        }
    }

    public class SnakeSizeLmlAttribute implements LmlBuildingAttribute<SnakeLmlActorBuilder> {
        @Override
        public Class<SnakeLmlActorBuilder> getBuilderType() {
            return SnakeLmlActorBuilder.class;
        }

        @Override
        public boolean process(final LmlParser parser, final LmlTag tag, final SnakeLmlActorBuilder builder,
                               final String rawAttributeData) {
            builder.setSize(parser.parseInt(rawAttributeData));
            return FULLY_PARSED;
        }
    }

    public class SnakeLmlActorBuilder extends LmlActorBuilder {
        private int size = 0;

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }
    }

    public static class ResLmlAttribute implements LmlAttribute<Image> {
        @Override
        public Class<Image> getHandledType() {
            return Image.class;
        }

        @Override
        public void process(final LmlParser parser, final LmlTag tag, final Image actor, final String rawAttributeData) {
            actor.setDrawable(ResourceService.getDrawable(parser.parseString(rawAttributeData)));
        }
    }

    public static class LmlAttributeDpValueWrapper extends AbstractCellLmlAttribute {
        private final AbstractCellLmlAttribute abstractCellLmlAttribute;

        public LmlAttributeDpValueWrapper(AbstractCellLmlAttribute abstractCellLmlAttribute) {
            this.abstractCellLmlAttribute = abstractCellLmlAttribute;
        }

        @Override
        public void process(LmlParser parser, LmlTag tag, Actor actor, Cell<?> cell, String rawAttributeData) {
            if(rawAttributeData.contains("dp")) {
                rawAttributeData = rawAttributeData.replace("dp","");
                rawAttributeData = String.valueOf(Float.parseFloat(rawAttributeData) * Gdx.graphics.getDensity());
            }
            abstractCellLmlAttribute.process(parser, tag, actor, cell, rawAttributeData);
        }
    }
}
