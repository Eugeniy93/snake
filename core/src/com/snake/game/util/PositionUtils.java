package com.snake.game.util;

public class PositionUtils {
    public static Point center(Point container, Point target) {
        return Point.of(container.x / 2 - target.x / 2, container.y / 2 - target.y / 2);
    }

//    public static Point bounds(Point position, Point size) {
//        return Point.of(container.x / 2 - target.x / 2, container.y / 2 - target.y / 2);
//    }
}
