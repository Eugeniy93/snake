package com.snake.game.util;

import java.util.Objects;

public class Point {
    public int x;
    public int y;

    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point p) {
        this.x = p.x;
        this.y = p.y;
    }

    public static Point of(int x, int y) {
        return new Point(x, y);
    }

    public static Point of(Point p) {
        return new Point(p);
    }

    public static Point neg(Point p) {
        return new Point(-p.x, -p.y);
    }

    public Point add(Point offset) {
        this.x += offset.x;
        this.y += offset.y;
        return this;
    }

    public Point sub(Point offset) {
        this.x -= offset.x;
        this.y -= offset.y;
        return this;
    }

    public Point set(Point p) {
        this.x = p.x;
        this.y = p.y;
        return this;
    }

    public Point set(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
