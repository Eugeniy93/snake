package com.snake.game.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class ResourceService {
    private static final float SP_64 = 64;
    private static final float SP_48 = 48;
    private static final float SP_36 = 36;
    private static final float SP_24 = 24;

    private static Skin cachedSkin; // TODO memory leak

    public static Skin getSkin() {
        if(cachedSkin != null) return cachedSkin;

        Skin skin = new Skin();

        FreeTypeFontGenerator.setMaxTextureSize(4096); // NOTE по умолчанию 1024 и на 144 размере появляется мусор
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/joystix.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.mono = true;

        parameter.size = (int) (SP_64 * Gdx.graphics.getDensity());
        skin.add("joystix-font-xlarge", generator.generateFont(parameter));

        parameter.size = (int) (SP_48 * Gdx.graphics.getDensity());
        skin.add("joystix-font-large", generator.generateFont(parameter));

        parameter.size = (int) (SP_36 * Gdx.graphics.getDensity());
        skin.add("joystix-font-medium", generator.generateFont(parameter));

        parameter.size = (int) (SP_24 * Gdx.graphics.getDensity());
        skin.add("joystix-font-small", generator.generateFont(parameter));

        skin.add("default", new Texture(0, 0, Pixmap.Format.RGBA8888));

        generator.dispose();

        skin.load(Gdx.files.internal("skin.json"));
        cachedSkin = skin;
        return skin;
    }

    public static Drawable getDrawable(String drawableResourceName) {
        final int destinySuffix = (int) Gdx.graphics.getDensity();
        final String destinyDependentPath = "drawable/x" + destinySuffix + "/" + drawableResourceName;
        final FileHandle destinyDependentRes = Gdx.files.internal(destinyDependentPath);
        if (destinyDependentRes.exists()) {
            return new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal(destinyDependentPath))));
        }

        final String destinyIndependentPath = "drawable/" + drawableResourceName;
        final FileHandle destinyIndependentRes = Gdx.files.internal(destinyIndependentPath);
        if(destinyIndependentRes.exists()) {
            return new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal(destinyIndependentPath))));
        }

        throw new IllegalArgumentException("Drawable " + drawableResourceName + " not found");
    }
}
