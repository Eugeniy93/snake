package com.snake.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.snake.game.SnakeApplication;

public class BaseScreen implements Screen {
    private final SnakeApplication application;

    public BaseScreen(SnakeApplication application) {
        this.application = application;
    }

    public SnakeApplication getApplication() {
        return application;
    }

    @Override
    public void show() {
        Gdx.app.debug(this.getClass().getSimpleName(), "show");
    }

    @Override
    public void render(float delta) {
    }

    @Override
    public void resize(int width, int height) {
        Gdx.app.debug(this.getClass().getSimpleName(), "resize");
    }

    @Override
    public void pause() {
        Gdx.app.debug(this.getClass().getSimpleName(), "pause");
    }

    @Override
    public void resume() {
        Gdx.app.debug(this.getClass().getSimpleName(), "resume");
    }

    @Override
    public void hide() {
        Gdx.app.debug(this.getClass().getSimpleName(), "hide");
    }

    @Override
    public void dispose() {
        Gdx.app.debug(this.getClass().getSimpleName(), "dispose");
    }
}
