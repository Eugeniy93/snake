package com.snake.game.screen;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.github.czyzby.lml.annotation.LmlAction;
import com.github.czyzby.lml.parser.impl.AbstractLmlView;

public class StartScreenView extends AbstractLmlView {
    private IStartScreenView listener;

    public StartScreenView() {
        super(new Stage());
    }

    public IStartScreenView getListener() {
        return listener;
    }

    public void setListener(IStartScreenView listener) {
        this.listener = listener;
    }

    @Override
    public String getViewId() {
        return this.getClass().getName();
    }

    @LmlAction("start")
    public void start(final TextButton actor) {
        if(listener != null) {
            listener.onStart();
        }
    }

    interface IStartScreenView {
        void onStart();
    }
}
