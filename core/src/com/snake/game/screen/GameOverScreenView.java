package com.snake.game.screen;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.github.czyzby.lml.annotation.LmlAction;
import com.github.czyzby.lml.annotation.LmlActor;
import com.github.czyzby.lml.parser.impl.AbstractLmlView;

public class GameOverScreenView extends AbstractLmlView {
    @LmlActor("scoreLabel")
    private Label scoreLabel;

    private IGameOverScreenView listener;

    public GameOverScreenView() {
        super(new Stage());
    }

    public IGameOverScreenView getListener() {
        return listener;
    }

    public void setListener(IGameOverScreenView listener) {
        this.listener = listener;
    }

    @Override
    public String getViewId() {
        return this.getClass().getName();
    }

    @LmlAction("tryAgain")
    public void tryAgain(final TextButton actor) {
        if(listener != null) {
            listener.onTryAgain();
        }
    }

    public void setScore(int score) {
        scoreLabel.setText(String.valueOf(score));
    }

    interface IGameOverScreenView {
        void onTryAgain();
    }
}
