package com.snake.game.screen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.snake.game.SnakeApplication;
import com.snake.game.engine.Direction;
import com.snake.game.engine.SnakeGame;
import com.snake.game.ui.SnakeWidget;


public class GameScreen extends LmlScreen<GameScreenView> implements GameScreenView.IGameScreenView {
    private SnakeGame snakeGame;

    public GameScreen(SnakeApplication application) {
        super(application, GameScreenView.class, "templates/game_screen.lml");
    }

    @Override
    public void show() {
        super.show();
        final SnakeWidget snakeWidget = getView().getSnakeWidget();
        final int baseSize = snakeWidget.getBaseSize();
//        float w = snakeWidget.getWidth();
//        float h = snakeWidget.getHeight();
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();
        final int minCellSize = (int) (Math.min(w, h) / baseSize);
        final int columns = (int) (w / minCellSize);
        final int lines = (int) (h / minCellSize);
        snakeGame = new SnakeGame(columns, lines);

        getView().setGame(snakeGame);
        getView().setScore(snakeGame.getScore());
        getView().setListener(this);
    }

    @Override
    public void onStep() {
        getView().setScore(snakeGame.getScore());
    }

        @Override
    public void onGameOver() {
        getApplication().setScreen(new GameOverScreen(getApplication(), snakeGame));
    }

    @Override
    public void onTap() {
        final Direction direction = snakeGame.getSnake().getDirection();
        switch (direction) {
            case UP:
                snakeGame.setAction(Direction.RIGHT);
                break;
            case DOWN:
                snakeGame.setAction(Direction.LEFT);
                break;
            case LEFT:
                snakeGame.setAction(Direction.UP);
                break;
            case RIGHT:
                snakeGame.setAction(Direction.DOWN);
                break;
        }
    }
}
