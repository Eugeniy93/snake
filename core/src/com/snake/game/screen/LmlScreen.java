package com.snake.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.github.czyzby.kiwi.util.gdx.asset.Disposables;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.impl.AbstractLmlView;
import com.github.czyzby.lml.util.Lml;
import com.snake.game.SnakeApplication;
import com.snake.game.ext.ExtLmlSyntax;
import com.snake.game.util.ResourceService;

public class LmlScreen<V extends AbstractLmlView> extends BaseScreen {
    private V view;
    private Class<V> viewClass;
    private String templatePath;


    public LmlScreen(SnakeApplication application, Class<V> viewClass, String templatePath) {
        super(application);
        this.viewClass = viewClass;
        this.templatePath=templatePath;
    }

    public V getView() {
        return view;
    }

    public void setView(V view) {
        this.view = view;
    }

    @Override
    public void show() {
        final LmlParser parser = constructParser();
        view = parser.createView(viewClass, Gdx.files.internal(templatePath));
        view.getStage().setDebugAll(false);
        Gdx.input.setInputProcessor(view.getStage());
    }

    private static LmlParser constructParser() {
        return Lml.parser()
                .syntax(new ExtLmlSyntax())
                .skin(ResourceService.getSkin())
                .build();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        view.render(delta);
    }

    @Override
    public void hide() {
        Disposables.disposeOf(view);
    }
}
