package com.snake.game.screen;

import com.snake.game.SnakeApplication;
import com.snake.game.engine.SnakeGame;


public class GameOverScreen extends LmlScreen<GameOverScreenView> implements GameOverScreenView.IGameOverScreenView {
    private SnakeGame snakeGame;

    public GameOverScreen(SnakeApplication application, SnakeGame snakeGame) {
        super(application, GameOverScreenView.class, "templates/game_over_screen.lml");
        this.snakeGame = snakeGame;
    }

    @Override
    public void show() {
        super.show();
        getView().setListener(this);
        getView().setScore(snakeGame.getScore());
    }

    @Override
    public void onTryAgain() {
        getApplication().setScreen(new GameScreen(getApplication()));
    }
}
