package com.snake.game.screen;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.github.czyzby.lml.annotation.LmlActor;
import com.github.czyzby.lml.annotation.LmlAfter;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.impl.AbstractLmlView;
import com.snake.game.engine.SnakeGame;
import com.snake.game.ui.SnakeWidget;
import com.snake.game.util.Point;

public class GameScreenView extends AbstractLmlView {
    @LmlActor("scoreLabel")
    private Label scoreLabel;

    @LmlActor("snakeWidget")
    private SnakeWidget snakeWidget;

    @LmlActor("stack")
    private Stack stack;

    private IGameScreenView listener;

    public GameScreenView() {
        super(new Stage());
    }

    @LmlAfter
    public void onCreate(final LmlParser parser) {
        stack.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (listener != null) {
                    listener.onTap();
                }
                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }

    public IGameScreenView getListener() {
        return listener;
    }

    public void setListener(IGameScreenView listener) {
        this.listener = listener;
    }

    @Override
    public String getViewId() {
        return this.getClass().getName();
    }

    public void setScore(int score) {
        scoreLabel.setText(String.valueOf(score));
    }

    public void setGame(SnakeGame game) {
        snakeWidget.setSnakeGame(game);
        snakeWidget.setListener(new SnakeWidget.SnakeWidgetListener() {

            private boolean isOverlaps() {
                final Rectangle scoreRect = new Rectangle(scoreLabel.getX(), scoreLabel.getY(),
                        scoreLabel.getWidth(), scoreLabel.getHeight());

                final Point cellSize = snakeWidget.getCellSize();
                for (Point p : snakeWidget.getSnakeGame().getSnake().getSegments()) {
                    Rectangle snakeSegmentRect = new Rectangle(p.x * cellSize.x, p.y * cellSize.y, cellSize.x, cellSize.y);
                    if (scoreRect.overlaps(snakeSegmentRect)) {
                        return true;
                    }
                }

                final Point foodPosition = snakeWidget.getSnakeGame().getFood();
                final Rectangle foodRect = new Rectangle(foodPosition.x * cellSize.x, foodPosition.y * cellSize.y, cellSize.x, cellSize.y);
                if (scoreRect.overlaps(foodRect)) {
                    return true;
                }

                return false;
            }


            @Override
            public void onStep() {
                if (isOverlaps()) {
                    scoreLabel.setColor(1f, 1f, 1f, 0.5f);
                } else {
                    scoreLabel.setColor(1f, 1f, 1f, 1f);
                }
                if (listener != null) {
                    listener.onStep();
                }
            }

            @Override
            public void onGameOver() {
                if (listener != null) {
                    listener.onGameOver();
                }
            }
        });
    }

    public SnakeWidget getSnakeWidget() {
        return snakeWidget;
    }

    interface IGameScreenView {
        void onTap();

        void onGameOver();

        void onStep();

    }
}
