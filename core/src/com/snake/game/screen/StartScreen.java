package com.snake.game.screen;

import com.badlogic.gdx.Game;
import com.snake.game.SnakeApplication;

public class StartScreen extends LmlScreen<StartScreenView> implements StartScreenView.IStartScreenView {
    public StartScreen(SnakeApplication application) {
        super(application, StartScreenView.class, "templates/start_screen.lml");
    }

    @Override
    public void show() {
        super.show();
        getView().setListener(this);
    }

    @Override
    public void onStart() {
        getApplication().setScreen(new GameScreen(getApplication()));
    }
}
