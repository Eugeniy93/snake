package com.snake.game;

import com.badlogic.gdx.Game;
import com.snake.game.screen.StartScreen;

public class SnakeApplication extends Game {
    @Override
    public void create() {
        setScreen(new StartScreen(this));
    }
}
