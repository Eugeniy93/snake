package com.snake.game.engine;

import com.snake.game.util.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Snake {
    private Direction direction;
    private List<Point> segments;

    public Snake(Point head, Direction direction) {
        this.direction = direction;
        this.segments = new ArrayList<>(Arrays.asList(head, new Point(head).sub(direction.getOffset())));
    }

    public void turn(Direction newDirection) {
        if (direction != Direction.neg(newDirection)) {
            direction = newDirection;
        }
    }

    public Point getHead() {
        return segments.get(0);
    }

    public List<Point> getSegments() {
        return segments;
    }

    public Direction getDirection() {
        return direction;
    }

    public void step() {
        final Point newHead = Point.of(getHead()).add(direction.getOffset());
        segments.add(0, newHead);
        segments.remove(segments.size() - 1);
    }

    public void feed() {
        segments.add(segments.get(segments.size() - 1));
    }
}
