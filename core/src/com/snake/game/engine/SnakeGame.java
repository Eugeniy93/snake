package com.snake.game.engine;

import com.badlogic.gdx.Gdx;
import com.snake.game.util.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SnakeGame {
    private Point size;
    private Point food;
    private Snake snake;
    private int stepCount;
    private Direction action;
    private boolean gameOver = false;

    private final List<Point> allPositions = new ArrayList<>();

    public SnakeGame(Point size) {
        setSize(size);
    }

    public SnakeGame(int columns, int lines) {
        setSize(Point.of(columns, lines));
    }

    public void restart() {
        setGameOver(false);
        stepCount = 0;
        action = Direction.UP;
        snake = new Snake(Point.of(size.x / 2, size.y / 2), action);
        prepareFood();
    }

    public void step() {
        if (size == null)
            throw new IllegalStateException("size must have a non null value. set the size before starting the game");
        if (isGameOver()) return;
        snake.turn(action);
        if (test(snake.getHead(), snake.getDirection())) {
            snake.step();
            stepCount++;
        } else {
            Gdx.input.vibrate(500); // TODO ->listener+config
//            Gdx.input.vibrate(new long[]{0,300,300,300,300,300}, -1); // TODO ->listener+config
            setGameOver(true);
        }
        if (snake.getHead().equals(food)) {
            Gdx.input.vibrate(100); // TODO ->listener+config
            snake.feed();
            prepareFood();
        }
    }

    private void prepareFood() {
        final List<Point> avaliablePositions = new ArrayList<>(allPositions);
        avaliablePositions.removeAll(snake.getSegments());
        food = Point.of(avaliablePositions.get(new Random().nextInt(avaliablePositions.size())));
    }

    private boolean test(Point head, Direction direction) {
        final Point testHead = Point.of(head).add(direction.getOffset());
        return testHead.x >= 0 && testHead.x < size.x &&
                testHead.y >= 0 && testHead.y < size.y &&
                !snake.getSegments().contains(testHead);
    }

    public int getStepCount() {
        return stepCount;
    }

    public void setSize(Point size) {
        this.size = size;
        allPositions.clear();
        for (int i = 0; i < size.x; i++) {
            for (int j = 0; j < size.y; j++) {
                allPositions.add(Point.of(i, j));
            }
        }
        restart();
    }

    public Point getSize() {
        return size;
    }

    public int getColumns() {
        return size.x;
    }

    public int getLines() {
        return size.y;
    }

    public Point getFood() {
        return food;
    }

    public Snake getSnake() {
        return snake;
    }

    public Direction getAction() {
        return action;
    }

    public void setAction(Direction action) {
        this.action = action;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public int getScore() {
        return snake.getSegments().size() - 2; // 2 - initial snake size
    }

}
