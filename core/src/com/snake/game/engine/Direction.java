package com.snake.game.engine;

import com.snake.game.util.Point;

public enum Direction {
    UP(new Point(0, 1)),
    DOWN(new Point(0, -1)),
    LEFT(new Point(-1, 0)),
    RIGHT(new Point(1, 0));

    private final Point offset;

    Direction(Point offset) {
        this.offset = offset;
    }

    public Point getOffset() {
        return offset;
    }

    public static Direction neg(Direction direction) {
        switch (direction) {
            case UP:
                return DOWN;
            case DOWN:
                return UP;
            case LEFT:
                return RIGHT;
            case RIGHT:
                return LEFT;
            default:
                throw new IllegalArgumentException("direction=" + direction);
        }
    }
}
